# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka                                        | vyjádření                       |
| :-------------------------------------------- | :------------------------------ |
| jak dlouho mi tvorba zabrala - **čistý čas**  | 9 hod                           |
| odkud jsem čerpal inspiraci                   | [inspirace](https://www.instructables.com/RGB-Lamp-WiFi/)  |
| odkaz na video                                | [Youtube](https://youtu.be/H3eR0R2l3bI)                     |
| jak se mi to podařilo rozplánovat             |  celkem dobře                               |
| proč jsem zvolil tento design                 |  líbil se mi                               |
| zapojení                                      | [schéma](https://gitlab.spseplzen.cz/skrziszowskif/4r-mood-lamp/-/blob/main/dokumentace/schema/schema.png)         |
| z jakých součástí se zapojení skládá          |  ARGB LED pásek, WeMos D1 mini, TP4056 nabíječka, baterie Li-Ion 18650, box na baterii, přepínač, kondenzátor, čidlo DHT11, vodiče                                |
| realizace                                     | [produkt](https://gitlab.spseplzen.cz/skrziszowskif/4r-mood-lamp/-/blob/main/dokumentace/fotky/konecnyProduktNoc.jpg) |
| UI                                            | [ui](https://gitlab.spseplzen.cz/skrziszowskif/4r-mood-lamp/-/blob/main/dokumentace/fotky/ui.JPG)                |
| co se mi povedlo                              |  uspořádat čas                               |
| co se mi nepovedlo/příště bych udělal/a jinak |  asi bych si nezničil wemos                               |
| zhodnocení celé tvorby (návrh známky)         |  práce se mi povedla 1                            |
