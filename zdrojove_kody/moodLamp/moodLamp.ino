#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <Adafruit_NeoPixel.h>
#include "ESPAsyncTCP.h"
#include "ESPAsyncWebServer.h"
#include "AsyncElegantOTA.h"
#include <DHT.h>

#define LED_PIN     D3
#define NUM_LEDS    30  
#define DHTPIN D1
#define DHTTYPE DHT11
DHT dht(DHTPIN,DHTTYPE);
//#define typDHT11 DHT11 
#define DELAYVAL 150
Adafruit_NeoPixel pixels(NUM_LEDS, LED_PIN, NEO_GRB + NEO_KHZ800);   
AsyncWebServer server(80);
const char* ssid = "3301-IoT";
const char* password = "mikrobus";
const char* mqtt_server = "mqtt-dashboard.com";

WiFiClient espClient;
PubSubClient client(espClient);
unsigned long lastMsg = 0;
#define MSG_BUFFER_SIZE	(50)
char msg[MSG_BUFFER_SIZE];
void setup_wifi() {
  //Připojení se k wifi
  delay(10);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  randomSeed(micros());
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send(200, "text/plain", "Hi! I am ESP8266.");
  });

  AsyncElegantOTA.begin(&server);    // Start ElegantOTA
  server.begin();
  Serial.println("HTTP server started");
}

void callback(char* topic, byte* payload, unsigned int length) {
   Serial.println("Přijata nová zpráva!");
  Serial.print("Téma: ");
  Serial.println(topic);

  StaticJsonDocument<64> doc;
  DeserializationError error = deserializeJson(doc, payload, length);
  if (error) {
    Serial.print("Chyba při parsování JSON: ");
    Serial.println(error.c_str());
    return;
  }

  byte r = doc["r"];
  byte g = doc["g"];
  byte b = doc["b"];

  Serial.println(r);
  setColor(r, g, b);

  switch (payload[0]) {
    case '0':
      setOff();
      break;
    case '1':
      setColor(255,0,0);
      break;
    case '2':
      pixelsShowByOne();
      break;
    case '3':
      disco();
      break;
    case '4':
      runningRGB();
      break;
    case '5':
      setColor(255,0,0);
      break;
    case '6':
      setColor(0,255,0);
      break;
    case '7':
      setColor(0,0,255);
      break;
    default:
      break;
  }
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      client.publish("skupina/4is2/skrziszowskif/ESP-JSON/welcome", "Hello!");
      // ... and resubscribe
      client.subscribe("skupina/4is2/skrziszowskif/ESP");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      delay(5000);
    }
  }
}

void setup() {
  pixels.begin();
  dht.begin();
  
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
}

void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  char buffer[350];
  JsonDocument doc;
  unsigned long now = millis();
   if (now - lastMsg > 3000) {
      doc["id"] = "ESP_Skrziszowski";
      doc["ip"] = WiFi.localIP();

      doc["teplota"]["hodnota"] = dht.readTemperature();
      doc["teplota"]["jednotka"] = "°C";
      doc["vlhkost"]["hodnota"] = dht.readHumidity();
      doc["vlhkost"]["jednotka"] = "%";

      size_t n = serializeJson(doc,buffer);
      client.publish("skupina/4is2/skrziszowskif/ESP-JSON/ESP",buffer,n);
      lastMsg = now;
  }
}
void runningRGB() {
    static int position = 0;
    for(int z = 0; z < 15; z++){
      for (int i = 0; i < NUM_LEDS; i++) {
          int red = 255 * (1 - abs(position - i) / (float)NUM_LEDS);
          int green = 255 * (1 - abs(position - i) / (float)NUM_LEDS);
          int blue = 255 * (1 - abs(position - i) / (float)NUM_LEDS);
          pixels.setPixelColor(i, pixels.Color(red, green, blue));
      }
      pixels.show(); // Aktualizace zobrazení
      position = (position + 1) % NUM_LEDS;
      delay(100); // Malá pauza pro efekt
    }
    
}
void disco() {
  for(int z = 0; z < 15; z++){
    for (int i = 0; i < NUM_LEDS; i++) {
        int red = random(256);   // Náhodná hodnota pro červenou složku
        int green = random(256); // Náhodná hodnota pro zelenou složku
        int blue = random(256);  // Náhodná hodnota pro modrou složku
        pixels.setPixelColor(random(NUM_LEDS), pixels.Color(red, green, blue));
      }
    pixels.show(); // Aktualizace zobrazení
    delay(200); // Malá pauza pro efekt
  }
      
}
void setOff(){
    for (int i = 0; i < NUM_LEDS; i++) {
        pixels.setPixelColor(i, pixels.Color(0, 0, 0)); // Černá barva vypíná LED
    }
    pixels.show(); // Aktualizace zobrazení
}
void pixelsShowByOne(){
  pixels.clear();
  for(int i=0; i<NUM_LEDS; i++) {
    pixels.setPixelColor(i, pixels.Color(0, 0, 150));
    pixels.show();  
    delay(DELAYVAL);
  }
}

void setColor(int red, int green, int blue) {
    for (int i = 0; i < NUM_LEDS; i++) {
        pixels.setPixelColor(i, pixels.Color(red, green, blue));
    }
    pixels.show();
}
